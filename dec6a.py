from functools import cache
from itertools import groupby
from sys import stdin

# def line_to_struct(stream, f=None):
#     return (line if f == None else f(line) for line in stream if line != "\n")



def f(initial, days):
    loops = 0
    school = str(initial)
    for _ in range(days):
        # print(school)
        new_school = ""
        for fish in school:
            loops += 1
            if fish == "0":
                new_school += "68"
            else:
                new_school += str(int(fish) - 1)
        school = new_school
    print("loops:", loops)
    return len(school)


@cache
def fr(initial, days):
    if days <= initial:
        return 1
    if initial > 0:
        return fr(0, days - initial)
    return fr(6, days - 1) + fr(8, days - 1)

initial_state = list((int(num) for num in stdin.readline().strip().split(",")))
print(initial_state)
fish_counts = ((group[0], len(list(group[1]))) for group in groupby(sorted(initial_state)))
print(fish_counts)

school_size = 0
for age_group in fish_counts:
    school_size += age_group[1] * fr(age_group[0], 256)

print("school size: ", school_size)

# for fg in fish_groups:
#     print(list(fg[1]))

# for

# print(f())

# from functools import cache


# def f_r(initial, days):
#     result = 0
#     for days_ in range(0, days, 5000):
#         result = fr(initial, days_)
#     return result