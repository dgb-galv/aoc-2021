"""
This is Dec 1, day #1 of the Advent of code, 2021.

It's all going down hill
"""
import sys

count = 0
last_reading = 10000
for raw_reading in sys.stdin:
    reading = int(raw_reading.strip())
    count += int(reading > last_reading)
    last_reading = reading

print(count)
