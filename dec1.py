#!/usr/bin/python3
"""
dec1.py

USAGE:
    dec1.py [--filter[=SMOOTHING_FACTOR]] [file ...]

DESCRIPTION:
    The dec1.py utility will process a batch of incoming
    raw sonar readings and return the magnitude of our
    subs' average downness vector over the course of the
    readings.

    The dec1.py utility has an optional filter which may
    be engaged by presenting the filter flag with the
    desired smoothing factor of 0 or greater.

    The dec1.py utility can take its readings from its
    standard input port through the mechanism which is
    common on your operating system or with its integrated
    diskfile reassembly sub-system.
"""

import sys


"""
Truly, an unknown... 

This represents our best guesses and latest measurements.
Please update as needed, but don't make it too big (???)
or the sonar blips won't come back...
"""
MAX_DEPTH_OF_OCEAN = 100000


"""
Read the configuration parameters and return a 
program run configuration structure.

If any invalid configuration parameters or invalid
combinations of configuration parameters are detected
the constant None will be returned.
"""


def get_opts(argv):
    args = argv
    if len(args) == 0:
        return (False, ())

    filter = 0
    if args[0] in ("-f", "--filter"):
        filter = True
    args = args[1:]

    for arg in args:
        if arg.startswith("-"):
            return None

    return (filter, args)


"""
Print out the runtime configuration options for
the system.
"""


def usage():
    print("dec1.py [--filter] [file ...]")


"""
Decode the raw, character-string encoded input file
into a stream of integer readings.
"""


def decode_stream(input):
    return (int(line.strip()) for line in input)


"""
A sliding-window buffer that can be quickly summed up
"""


class SumBuffer:
    head = None
    tail = None

    class BufferNode:
        next = None
        val = 0

        def __init__(self, val):
            self.val = val

    def push(self, val):
        if self.tail == None:
            self.tail = self.BufferNode(val)
            self.head = self.tail
        else:
            self.tail.next = self.BufferNode(val)
            self.tail = self.tail.next

    def rotate(self, val):
        self.push(val)
        self.head = self.head.next

    def sum(self):
        sum = 0
        node = self.head
        while node:
            sum += node.val
            node = node.next
        return sum


"""
Apply the input smoothing filter.

The smoothing factor is the number of readings that
are grouped together to cause the smoothing effect.
The smoothing factor should be 0 or greater with
0 causing zero filtering effect.
"""

filter_buffer = SumBuffer()


def apply_smoothing_filter(input_signal, smoothing_factor=2):
    if smoothing_factor < 0:
        smoothing_factor = 0

    for i in range(smoothing_factor + 1):
        reading = next(input_signal, None)
        if reading == None:
            return
        filter_buffer.push(reading)

    yield filter_buffer.sum()

    for reading in input_signal:
        filter_buffer.rotate(reading)
        yield filter_buffer.sum()


"""
Utilize the integrated disk-file reassembly sub-system
to reassemble the disk-files into a coherent stream
for further processing.
"""


def assemble_packets(files):
    for file in files:
        with open(file, "r") as f:
            for line in f:
                yield line


"""
Program Runtime Life-Cycle:
1) Read and verify runtime configuration
2) Configure integrated disk-file reassembly sub-system
3) Configure downness vector smoothing filter
4) Process input sonar readings
5) Output analysis report
"""


def main(argv):
    opts = get_opts(argv)
    if opts == None:
        usage()
        sys.exit(2)

    if len(opts[1]) > 0:
        input_signal = assemble_packets(opts[1])
    else:
        input_signal = sys.stdin

    input_signal = decode_stream(input_signal)

    if opts[0]:
        input_signal = apply_smoothing_filter(input_signal)

    count = 0
    last_reading = MAX_DEPTH_OF_OCEAN
    for reading in input_signal:
        count += int(reading > last_reading)
        last_reading = reading

    print(count)


if __name__ == "__main__":
    main(sys.argv[1:])
